import React from "react";

const Item = (props) => {
  const { product, handlePrdDetail } = props;
  console.log("handlePrdDetail: ", handlePrdDetail);
  return (
    <div className="col-4 mt-3">
      <div className="card">
        <img src={product.image} alt="..." />
        <div className="card-body">
          <p className="font-weight-bold">{product.name}</p>
          <p className="mt-3">{product.price} $</p>
          <button
            className="btn btn-outline-success mt-3"
            data-toggle="modal"
            data-target="#exampleModal"
            onClick={() => handlePrdDetail(product)}
          >
            Xem chi tiết
          </button>
        </div>
      </div>
    </div>
  );
};

export default Item;
