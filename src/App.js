import "./App.css";
import BTShoes from "./BTShoes/BTShoes";

function App() {
  return (
    <div>
      <BTShoes />
    </div>
  );
}

export default App;
