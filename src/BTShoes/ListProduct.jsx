import React from 'react'
import Item from './Item'

const ListProduct = (props) => {
    const { data, handlePrdDetail } = props
    console.log('data: ', data)
    return (
        <div className="row">
            {/* C1 */}
            {/* {data.map((item, index) => {
                console.log('item: ',index, item);
                return <Item product={item} key={item.id} />
            })} */}

            {/* C2 */}
            {data.map((item, index) => (
                <Item handlePrdDetail={handlePrdDetail} product={item} key={item.id} />
            ))}
        </div>
    )
}

export default ListProduct
